package ipfsDocs

import (
	"github.com/ipfs/go-ipfs-api"
	"io"
	"io/ioutil"
)

type DocumentsShell interface {
	// Add file and return the path/address
	Add(io.Reader) (string, error)

	// Reads all the bytes from the given path
	Cat(string) ([]byte, error)
}

type IpfsShell struct {
	Sh *shell.Shell
}

func (s *IpfsShell) Add(r io.Reader) (string, error) {
	return s.Sh.Add(r)
}

func (s *IpfsShell) Cat(path string) ([]byte, error) {
	rawReader, err := s.Sh.Cat(path)
	if err != nil {
		return nil, err
	}

	raw, err := ioutil.ReadAll(rawReader)
	if err != nil {
		return nil, err
	}

	if err = rawReader.Close(); err != nil {
		return nil, err
	}

	return raw, nil
}

func NewIpfsShell(addr string) *IpfsShell {
	sh := shell.NewShell(addr)
	return &IpfsShell{Sh: sh}
}
