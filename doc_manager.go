package ipfsDocs

import (
	"bytes"
	"encoding/json"
	"errors"
)

// This is for managing ipfs documents
type IpfsDocManager struct {
	Shell DocumentsShell
	Docs  map[string]string
}

func (dm *IpfsDocManager) AddDocument(path string, src Document) (*SavedDocumentData, error) {
	if len(src) == 0 {
		return nil, errors.New("content must not be empty")
	}

	docHtml, err := src.RenderToHtml()
	if err != nil {
		return nil, err
	}

	pubHash, err := dm.Shell.Add(bytes.NewReader(docHtml))
	if err != nil {
		return nil, err
	}

	ssd := &SavedDocumentData{
		Path:     path,
		PubHash:  pubHash,
		Source:   string(src),
		PrevHash: "",
	}

	jsonBytes, err := json.Marshal(ssd)
	if err != nil {
		return nil, err
	}

	savedDocHash, err := dm.Shell.Add(bytes.NewReader(jsonBytes))
	if err != nil {
		return nil, err
	}

	dm.Docs[path] = savedDocHash

	return ssd, nil
}

func (dm *IpfsDocManager) RoutesToJson() ([]byte, error) {
	allRoutes := make([]DocumentHash, 0)
	for k, v := range dm.Docs {
		allRoutes = append(allRoutes, DocumentHash{Path: k, IpfsPath: v})
	}
	jsonByte, err := json.Marshal(allRoutes)
	if err != nil {
		return nil, err
	}

	return jsonByte, nil
}

func NewDocManager(sh DocumentsShell) (*IpfsDocManager, error) {
	return &IpfsDocManager{
		Shell: sh,
		Docs:  make(map[string]string),
	}, nil
}

func DocManagerFromJson(sh DocumentsShell, jsonRoutes []byte) (*IpfsDocManager, error) {
	var allRoutes []*DocumentHash
	err := json.Unmarshal(jsonRoutes, &allRoutes)
	if err != nil {
		return nil, err
	}

	docs := make(map[string]string)
	for _, r := range allRoutes {
		docs[r.Path] = r.IpfsPath
	}

	return &IpfsDocManager{
		Shell: sh,
		Docs:  docs,
	}, nil
}
