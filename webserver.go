package ipfsDocs

import (
	"encoding/json"
	"errors"
	"io/ioutil"
	"log"
	"net/http"
	"strings"
)

type WebServer struct {
	// File where the routes are stored. This must be an absolute path
	RoutesPath string

	// Maps the path to an ipfs hash
	AllRoutes map[string]string

	// Web server
	Server *http.Server

	// IPFS shell
	Shell DocumentsShell
}

type HandleFunc func(http.ResponseWriter, *http.Request)

func (ws *WebServer) reloadRoutes() error {
	routesSrc, err := ioutil.ReadFile(ws.RoutesPath)
	if err != nil {
		return err
	}

	var allHashes []DocumentHash
	err = json.Unmarshal(routesSrc, &allHashes)
	if err != nil {
		return err
	}

	allRoutes := make(map[string]string)
	for _, h := range allHashes {
		allRoutes[h.Path] = h.IpfsPath
	}

	ws.AllRoutes = allRoutes
	log.Println(ws.AllRoutes)
	return nil
}

func (ws *WebServer) reloadHandler() HandleFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		log.Println("request to reload")
		err := ws.reloadRoutes()
		if err != nil {
			writeResponse(w, err.Error(), http.StatusInternalServerError)
			return
		}
	}
}

func (ws *WebServer) getDocument(path string) ([]byte, error) {
	hash, ok := ws.AllRoutes[path]
	if !ok {
		return nil, errors.New("path not found")
	}

	log.Printf("Fetching %s from ipfs. ", hash )
	jsonDoc, err := ws.Shell.Cat(hash)
	if err != nil {
		return nil, err
	}
	log.Println("Done.")

	var sdd *SavedDocumentData
	err = json.Unmarshal(jsonDoc, &sdd)
	if err != nil {
		return nil, err
	}

	log.Printf("Fetching %s from ipfs. ", sdd.PubHash )
	pubDoc, err := ws.Shell.Cat(sdd.PubHash)
	if err != nil {
		return nil, err
	}
	log.Println("Done.")

	return pubDoc, nil
}

func (ws *WebServer) getHandler() HandleFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		trimmedUrl := strings.Trim(r.URL.Path, "/")
		if len(trimmedUrl) == 0 {
			writeResponse(w, "bad request", http.StatusBadRequest)
			return
		} else {
			log.Printf("request to %s\n", trimmedUrl)
			if r.Method != http.MethodGet {
				writeResponse(w, "method not allowed", http.StatusMethodNotAllowed)
				return
			}

			if len(strings.Split(trimmedUrl, "/")) > 1 {
				writeResponse(w, "malformed url", http.StatusBadRequest)
				return
			}

			pubDocHtml, err := ws.getDocument(trimmedUrl)
			if err != nil {
				writeResponse(w, err.Error(), http.StatusInternalServerError)
				return
			}

			_, err = w.Write(pubDocHtml)
			if err != nil {
				log.Println("err writing response: " + err.Error())
			}
		}
	}
}

func (ws *WebServer) SetupWebServer(addr string) error {
	mux := http.NewServeMux()

	mux.HandleFunc("/reload", ws.reloadHandler())
	mux.HandleFunc("/", ws.getHandler())

	ws.Server = &http.Server{Addr: addr, Handler: mux}

	return nil
}

func (ws *WebServer) Serve() error {
	return ws.Server.ListenAndServe()
}

func NewWebServer(addr, routesFile string, shell DocumentsShell) (*WebServer, error) {
	ws := &WebServer{
		RoutesPath: routesFile,
		AllRoutes:  make(map[string]string),
		Shell:      shell,
	}

	err := ws.SetupWebServer(addr)
	if err != nil {
		return nil, err
	}

	if err = ws.reloadRoutes(); err != nil {
		return nil, err
	}

	return ws, nil
}

func writeResponse(writer http.ResponseWriter, msg string, code int) {
	if code != http.StatusOK {
		writer.WriteHeader(code)
	}
	writer.Write([]byte(msg))
}
