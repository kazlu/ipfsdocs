package ipfsDocs

import (
	"bytes"
	"fmt"
	"io/ioutil"
	"log"
	"os/exec"
	"path/filepath"
)

type DocumentUploader struct {
	Host      string
	User      string
	FilePath  string
	RemoteDir string
	Dm        *IpfsDocManager
}

func (du *DocumentUploader) AddDocument(path, content string) (error) {
	_, err := du.Dm.AddDocument(path, Document(content))
	if err != nil {
		return err
	}
	return nil
}

func (du *DocumentUploader) UploadRoutes() error {
	absLocalFile, err := filepath.Abs(du.FilePath)
	if err != nil {
		return err
	}
	du.SaveRoutesJson()
	if err = du.SaveRoutesJson(); err != nil {
		return err
	}

	fileName := filepath.Base(absLocalFile)
	remotePath := fmt.Sprintf("%s@%s:%s/%s", du.User, du.Host, du.RemoteDir, fileName)
	log.Printf("executing 'scp %s %s' \n", du.FilePath, remotePath)
	cmd := exec.Command("scp", du.FilePath, remotePath)

	var stderr bytes.Buffer
	cmd.Stderr = &stderr
	err = cmd.Run()
	if err != nil {
		return fmt.Errorf("%s: %s", err, stderr.String())
	}
	return nil
}
func (du *DocumentUploader) SaveRoutesJson() error {
	jsonContent, err := du.Dm.RoutesToJson()
	if err != nil {
		return err
	}
	return ioutil.WriteFile(du.FilePath, jsonContent, 066)
}

func NewDocumentUploader(host, user, localFile, remoteDir string, dm *IpfsDocManager) (*DocumentUploader, error) {
	du := &DocumentUploader{
		Host:      host,
		User:      user,
		FilePath:  localFile,
		RemoteDir: remoteDir,
		Dm:        dm,
	}

	return du, nil
}
