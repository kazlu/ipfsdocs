package ipfsDocs

import (
	"bytes"
	"crypto"
	"golang.org/x/crypto/openpgp"
	"golang.org/x/crypto/openpgp/armor"
	"golang.org/x/crypto/openpgp/packet"
	"io/ioutil"
)

type Encrypter struct {
	Entities openpgp.EntityList
}

// Armor the private key in index
func (e *Encrypter) ArmoredPrivateKey(idx int) (string, error) {
	buffer := new(bytes.Buffer)
	w, err := armor.Encode(buffer, openpgp.PrivateKeyType, nil)
	if err != nil {
		return "", err
	}

	err = e.Entities[idx].SerializePrivate(w, nil)
	if err != nil {
		return "", nil
	}

	err = w.Close()
	if err != nil {
		return "", err
	}

	bts, err := ioutil.ReadAll(buffer)
	if err != nil {
		return "", err
	}
	return string(bts), nil
}

// Armor the private key in index
func (e *Encrypter) ArmorPublicKey(idx int) (string, error) {
	buffer := new(bytes.Buffer)
	w, err := armor.Encode(buffer, openpgp.PublicKeyType, nil)
	if err != nil {
		return "", err
	}

	err = e.Entities[idx].Serialize(w)
	if err != nil {
		return "", nil
	}

	err = w.Close()
	if err != nil {
		return "", err
	}

	bts, err := ioutil.ReadAll(buffer)
	if err != nil {
		return "", err
	}
	return string(bts), nil
}

func (e *Encrypter) EncryptText(s []byte) ([]byte, error) {
	buffer := new(bytes.Buffer)
	w, err := openpgp.Encrypt(buffer, e.Entities, nil, nil, nil)
	if err != nil {
		return nil, err
	}

	_, err = w.Write(s)
	if err != nil {
		return nil, err
	}
	err = w.Close()
	if err != nil {
		return nil, err
	}

	byts, err := ioutil.ReadAll(buffer)
	if err != nil {
		return nil, err
	}

	return byts, nil
}

func (e *Encrypter) DecryptText(enc []byte) ([]byte, error) {
	md, err := openpgp.ReadMessage(bytes.NewReader(enc), e.Entities, nil, nil)
	if err != nil {
		return nil, err
	}

	content, err := ioutil.ReadAll(md.UnverifiedBody)
	if err != nil {
		return nil, err
	}

	return content, nil
}

func NewEncrypter() (*Encrypter, error) {
	cfg := &packet.Config{
		DefaultHash: crypto.SHA256,
	}
	entity, err := openpgp.NewEntity("name", "asdf", "em@ai.il", cfg)
	if err != nil {
		return nil, err
	}

	buffer := new(bytes.Buffer)
	err = entity.SerializePrivate(buffer, cfg)
	if err != nil {
		return nil, err
	}

	entList, err := openpgp.ReadKeyRing(buffer)
	if err != nil {
		return nil, err
	}

	return &Encrypter{Entities: entList}, nil
}

func LoadEncrypter(rawPrivate string) (*Encrypter, error) {
	entList, err := openpgp.ReadArmoredKeyRing(bytes.NewReader([]byte(rawPrivate)))
	if err != nil {
		return nil, err
	}

	return &Encrypter{Entities: entList}, nil
}
