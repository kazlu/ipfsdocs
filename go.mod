module gitlab.com/kazlu/ipfsDocs

require (
	github.com/agl/ed25519 v0.0.0-20170116200512-5312a6153412 // indirect
	github.com/btcsuite/btcd v0.0.0-20180924021209-2a560b2036be // indirect
	github.com/cheekybits/is v0.0.0-20150225183255-68e9c0620927 // indirect
	github.com/davecgh/go-spew v1.1.1 // indirect
	github.com/gofrs/uuid v3.1.0+incompatible
	github.com/gogo/protobuf v1.1.1 // indirect
	github.com/gxed/hashland v0.0.0-20180221191214-d9f6b97f8db2 // indirect
	github.com/ipfs/go-ipfs-api v1.3.5
	github.com/ipfs/go-ipfs-cmdkit v1.1.3 // indirect
	github.com/libp2p/go-flow-metrics v0.2.0 // indirect
	github.com/libp2p/go-libp2p-crypto v2.0.1+incompatible // indirect
	github.com/libp2p/go-libp2p-metrics v2.1.6+incompatible // indirect
	github.com/libp2p/go-libp2p-peer v2.3.8+incompatible // indirect
	github.com/libp2p/go-libp2p-protocol v1.0.0 // indirect
	github.com/libp2p/go-libp2p-pubsub v0.1.0 // indirect
	github.com/microcosm-cc/bluemonday v1.0.1
	github.com/minio/blake2b-simd v0.0.0-20160723061019-3f5f724cb5b1 // indirect
	github.com/minio/sha256-simd v0.0.0-20171213220625-ad98a36ba0da // indirect
	github.com/mitchellh/go-homedir v1.0.0 // indirect
	github.com/mr-tron/base58 v0.0.0-20180922112544-9ad991d48a42 // indirect
	github.com/multiformats/go-multiaddr v1.3.0 // indirect
	github.com/multiformats/go-multiaddr-net v1.6.3 // indirect
	github.com/multiformats/go-multihash v1.0.8 // indirect
	github.com/russross/blackfriday v2.0.0+incompatible
	github.com/shurcooL/sanitized_anchor_name v0.0.0-20170918181015-86672fcb3f95 // indirect
	github.com/spaolacci/murmur3 v0.0.0-20180118202830-f09979ecbc72 // indirect
	github.com/whyrusleeping/tar-utils v0.0.0-20180509141711-8c6c8ba81d5c // indirect
	golang.org/x/crypto v0.0.0-20180927165925-5295e8364332
	golang.org/x/net v0.0.0-20180926154720-4dfa2610cdf3 // indirect
	golang.org/x/sys v0.0.0-20180928133829-e4b3c5e90611 // indirect
)
