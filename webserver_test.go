package ipfsDocs
//
//import (
//	"bytes"
//	"encoding/json"
//	"fmt"
//	"io/ioutil"
//	"net/http"
//	"net/http/httptest"
//	"strings"
//	"testing"
//)
//
//type MockDocManager struct {
//	paramRec Document
//}
//
//func (mdm *MockDocManager) AddDocument(d Document) (*SavedDocumentData, error) {
//	mdm.paramRec = d
//	return &SavedDocumentData{"uuid", "", "pubHash", string(d)}, nil
//}
//
//func (mdm *MockDocManager) UpdateDocument(tokenKey string, d Document) (*SavedDocumentData, error) {
//	return &SavedDocumentData{tokenKey, "", "pubHash", string(d)}, nil
//}
//
//func (mdm *MockDocManager) GetDocument(tokenKey string) (*SavedDocumentData, error) {
//	return &SavedDocumentData{tokenKey, "", "pubHash", ""}, nil
//}
//
//// test in base64
//const toSend = `
//{
//	"content": "dGVzdAo="
//}`
//
//func TestSaveHandler(t *testing.T) {
//	mockDoc := &MockDocManager{}
//	ws, err := NewWebServer(mockDoc)
//	ok(t, err)
//	ws.Mux.HandleFunc("/", ws.newDocument)
//	testServer := httptest.NewServer(ws.Mux)
//	defer testServer.Close()
//
//	res, err := http.Post(testServer.URL + "/andres/ruiz",
//		"application/json", bytes.NewReader([]byte(toSend)))
//	ok(t, err)
//
//	if http.StatusOK != res.StatusCode {
//		t.Errorf("expected status code 200, got %d", res.StatusCode)
//	}
//
//	//if !bytes.Equal([]byte("test"), mockDoc.paramRec) {
//	//	t.Errorf("expected <test> got <%s>", mockDoc.paramRec)
//	//}
//
//	resByte, err := ioutil.ReadAll(res.Body)
//	var response *SavedDocumentData
//
//	json.Unmarshal(resByte, &response)
//	if "uuid" != response.Path {
//		t.Errorf("Expected uuid got %s", response.Path)
//	}
//
//	//if "hash" != response.Hash {
//	//	t.Errorf("Expected hash got %s", response.Hash)
//	//}
//
//	if "pubHash" != response.PubHash {
//		t.Errorf("Expected pubHash got %s", response.PubHash)
//	}
//}
