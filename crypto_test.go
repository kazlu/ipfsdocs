package ipfsDocs

import (
	"bytes"
	"strings"
	"testing"
)

func TestKeyPairGeneration(t *testing.T) {
	// Arrange
	const keyStrStart = "-----BEGIN PGP PRIVATE KEY BLOCK-----"
	const keyStrEnd = "-----END PGP PRIVATE KEY BLOCK-----"
	encry, err := NewEncrypter()
	ok(t, err)
	privKey, err := encry.ArmoredPrivateKey(0)
	ok(t, err)

	if strings.Index(privKey, keyStrStart) != 0 {
		t.Errorf("Key must start with %s", keyStrStart)
	}

	if strings.Index(privKey, keyStrEnd) == -1 {
		t.Errorf("Key must start with %s", keyStrStart)
	}
}

func TestLoadArmoredPrivateKey(t *testing.T) {
	// Arrange
	enc, err := NewEncrypter()
	ok(t, err)
	priv, err := enc.ArmoredPrivateKey(0)
	ok(t, err)

	// Act
	_, err = LoadEncrypter(priv)
	ok(t, err)
}

func TestEncryptDecryptSameEncrypterMessage(t *testing.T) {
	secret := []byte("this is a secret")
	encry, err := NewEncrypter()
	ok(t, err)

	msgEncrypted, err := encry.EncryptText(secret)
	ok(t, err)

	if len(msgEncrypted) < 0 {
		t.Error("Expected a string. Got none")
	}

	decrypted, err := encry.DecryptText(msgEncrypted)
	ok(t, err)

	if !bytes.Equal(decrypted, secret) {
		t.Errorf("Expected <%s> got <%s>", decrypted, secret)
	}
}

func TestEncryptDecryptDifferentEncryptersMessage(t *testing.T) {
	secret := []byte("this is a secret")
	enc1, err := NewEncrypter()
	ok(t, err)

	arm, err := enc1.ArmoredPrivateKey(0)
	enc2, err := LoadEncrypter(arm)
	ok(t, err)

	msgEncrypted, err := enc1.EncryptText(secret)
	ok(t, err)

	if len(msgEncrypted) < 0 {
		t.Error("Expected a string. Got none")
	}

	decrypted, err := enc2.DecryptText(msgEncrypted)
	ok(t, err)

	if !bytes.Equal(decrypted, secret) {
		t.Errorf("Expected %s got %s", decrypted, secret)
	}
}

func ok(t *testing.T, err error) {
	if err != nil {
		t.Error(err)
	}
}
