package ipfsDocs

import (
	"encoding/json"
	"fmt"
	"io"
	"io/ioutil"
	"strings"
	"testing"
)

type MockShell struct {
	paramHistory []string
}

func (m *MockShell) Add(r io.Reader) (string, error) {
	bt, err := ioutil.ReadAll(r)
	if err != nil {
		return "", err
	}
	m.paramHistory = append(m.paramHistory, string(bt))
	id := fmt.Sprintf("id%d", len(m.paramHistory))

	return id, nil
}

func (m *MockShell) Cat(tokenKey string) ([]byte, error) {
	return nil, nil
}

var mockShell *MockShell

func makeDocManager() (*IpfsDocManager, error) {
	mockShell = &MockShell{make([]string, 0)}
	return NewDocManager(mockShell)
}

func TestIpfsDocManager_AddDocument(t *testing.T) {
	dm, err := makeDocManager()
	ok(t, err)

	// Test in base64
	src := Document("test")
	savedDoc, err := dm.AddDocument("test-path", src)
	ok(t, err)

	if "id1" != savedDoc.PubHash {
		t.Errorf("expected <id1> got <%s> as hash", savedDoc.PubHash)
	}

	if "id2" != dm.Docs["test-path"] {
		t.Errorf("expected doc to be <id2> got <%s> as hash", dm.Docs["test-path"])
	}

	if strings.Index(mockShell.paramHistory[0], "<p>test</p>") == -1 {
		t.Error("expected '<p>test</p>' to be in param sent to ipfs.")
	}

	const jsonResponse = `
{
	"path": "test-path",
	"prev_hash": "",
	"published_hash": "id1",
	"source": "test"
}
`
	var expectedDoc *SavedDocumentData
	json.Unmarshal([]byte(jsonResponse), &expectedDoc)

	if !savedDoc.Equals(expectedDoc) {
		t.Errorf("Expected %s. Got %s", expectedDoc, savedDoc)
	}
}

func TestIpfsDocManager_AddDocument2(t *testing.T) {
	sh := NewIpfsShell("localhost:5001")
	dm, err := NewDocManager(sh)
	ok(t, err)

	_, err = dm.AddDocument("probando", "# hey ## sexy *asdf*")
	ok(t, err)

	allRoutes, err := dm.RoutesToJson()
	ok(t, err)

	fmt.Println(allRoutes)
}


