package main

import (
	"encoding/base64"
	"fmt"
	"io/ioutil"
	"os"
)

func main() {
	inStream := os.Stdin
	outStream := os.Stderr

	a, err := ioutil.ReadAll(inStream)
	if err != nil {
		fmt.Fprint(outStream, err)
		return
	}

	fmt.Println(base64.StdEncoding.EncodeToString(a))
}
