package main

import (
	"flag"
	"fmt"
	"gitlab.com/kazlu/ipfsDocs"
	"os"
	"path/filepath"
)

func main() {
	sh := ipfsDocs.NewIpfsShell("localhost:5001")

	routeFile := flag.String("routes", "", "file for the routes")

	flag.Parse()

	if *routeFile == "" {
		fmt.Fprintln(os.Stderr, "routes required")
		flag.Usage()
		return
	}

	if !pathExists(*routeFile) {
		fmt.Fprintln(os.Stderr, "path for routes doesn't exist")
		return
	}

	ws, err := ipfsDocs.NewWebServer("localhost:8421", *routeFile, sh)
	if err != nil {
		fmt.Fprintln(os.Stderr, err)
		return
	}

	err = ws.Serve()
	if err != nil {
		fmt.Fprintln(os.Stderr, err)
	}
}

func pathExists(path string) bool {
	absPath, err := filepath.Abs(path)
	if err != nil {
		return false
	}

	_, err = os.Stat(absPath)
	if err != nil {
		return false
	}

	return true
}
