package main

import "os/exec"

func StartServer() {
	cmd := exec.Command("ipfs", "daemon")
	err := cmd.Run()

	if err != nil {
		panic(err)
	}
}

func main() {
	StartServer()
}
