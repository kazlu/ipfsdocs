package main

import (
	"flag"
	"fmt"
	"gitlab.com/kazlu/ipfsDocs"
	"io/ioutil"
	"os"
)

func main() {
	user := "kazlu"
	host := "188.166.16.56"
	localFile := "/Users/afruizc/Documents/code/ipfsdocs/routes.json"

	sh := ipfsDocs.NewIpfsShell("localhost:5001")
	dm, err := ipfsDocs.DocManagerFromJson(sh, loadJson(localFile))
	if err != nil {
		panic(err)
	}

	remoteDir := fmt.Sprintf("/home/%s/sites/ipfsdocs", user)
	du, err := ipfsDocs.NewDocumentUploader(host, user, localFile, remoteDir, dm)
	if err != nil {
		panic(err)
	}

	path := flag.String("path", "", "[required] path of the file")
	contentFile := flag.String("content", "", "[required] contents to upload")

	flag.Parse()

	if *path == "" {
		fmt.Fprintln(os.Stderr, "path required")
		flag.Usage()
		return
	}

	if *contentFile == "" {
		fmt.Fprintln(os.Stderr, "contentFile required")
		flag.Usage()
		return
	}

	contents, err := ioutil.ReadFile(*contentFile)
	if err != nil {
		panic(err)
	}

	err = du.AddDocument(*path, string(contents))
	if err != nil {
		panic(err)
	}

	err = du.UploadRoutes()
	if err != nil {
		panic(err)
	}

	fmt.Fprintln(os.Stdout, "File successfully updated.")
}

func loadJson(jsonPath string) []byte {
	jsonByte, err := ioutil.ReadFile(jsonPath)
	if err != nil {
		panic(err)
	}

	return jsonByte
}
