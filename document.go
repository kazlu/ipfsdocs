package ipfsDocs

import (
	"fmt"
	"github.com/microcosm-cc/bluemonday"
	"github.com/russross/blackfriday"
	"io/ioutil"
	"regexp"
)

const TemplatePath = BaseDir + "/assets/page_template.html"

type Document string

func (d Document) RenderToHtml() ([]byte, error) {
	templateContent, err := ioutil.ReadFile(TemplatePath)
	if err != nil {
		return nil, err
	}
	strContent := string(templateContent)

	usrContent, err := d.parseContent()
	if err != nil {
		return nil, err
	}

	return []byte(fmt.Sprintf(strContent, "", usrContent)), nil
}

func (d Document) parseContent() (string, error) {
	dirty := blackfriday.Run([]byte(d))
	policy := bluemonday.UGCPolicy()
	policy.AllowAttrs("class").Matching(
		regexp.MustCompile("^language-[a-zA-Z0-9]+$")).OnElements("code")
	sanitized := policy.Sanitize(string(dirty))
	return sanitized, nil
}

// The source content of a document. Generally in markdown
type SavedDocumentData struct {
	Path     string `json:"path"`
	PrevHash string `json:"prev_hash"`
	PubHash  string `json:"published_hash"`
	Source   string `json:"source"`
}

func (data *SavedDocumentData) Equals(data2 *SavedDocumentData) bool {
	if data.Path != data2.Path {
		return false
	}

	if data.PrevHash != data2.PrevHash {
		return false
	}

	if data.PubHash != data2.PubHash {
		return false
	}

	if data.Source != data2.Source {
		return false
	}

	return true
}

func (data *SavedDocumentData) String() string {
	return fmt.Sprintf("uuid: %s\nprev_hash: %s\npub_hash: %s\nsource: %s\n",
		data.Path, data.PrevHash, data.PubHash, data.Source)
}

// Hash and path of a document
type DocumentHash struct {
	Path     string `json:"path"`
	IpfsPath string `json:"ipfs_path"`
}
