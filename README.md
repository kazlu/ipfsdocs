# markdownPages
markdownPages is a way of keeping a blog for yourself supported by ipfs. markdownPages
helps you create pages like [this one](https://cloudflare-ipfs.com/ipfs/QmfFg4ypPSKBJvrMuzKUE713azeAVEXJXs2b4AiHeYFJbM).
For more info please see this blog post:
[IFPS and decentralization](https://ruizandr.es/markdownPages/h)

**Be advised, this is currently in experimental mode. Do not use it in production.**

# Prerequisites

You need to have go >= 1.11 and ipfs in order to run markdownPages. In addition, your `ipfs daemon`
[should be running](https://docs.ipfs.io/introduction/usage/#going-online).

## Installing on Linux

(danger zone) This instructions are not tested!!
```bash
go mod vendor
```

# Usage
There is an http server used to retrieve the pages from ipfs. You need to tell
this server the hashes of the ipfs documents so that they can be retrieved. In the
current iteration of markdownPages we use a file named `pages.json` to indicate
the url of the page and the hash it maps to. The file structure is as follows:

```json
[
  {
    "path": "string",
    "ipfs_hash": "string"
  },
  {
    "path": "string",
    "ipfs_hash": "string"
  }
]
```

The `path` must not contain any "/" or any strange characters. Once the http server
gets a request, it searches the list for the corresponding ipfs. It then retrieves
the file using this hash and renders it, returning as the response the rendered
html document.

### Endpoints
This is very much a work in progress. It is expect to change to a more standard
interface.

**SavedDocument struct:** all the endpoints return a struct that represents a
saved document:

```json
SavedDocument = {
  "uuid": "string",
  "source": "base64 encoded of source",
  "published_hash": "string",
  "prev_hash": "string",
}
```
`uuid` is the unique id assigned to this document. `source` is the base64
decoded source for the document (for now we always asume this is markdown).
`published_hash` is the ipfs hash of the html markdown rendered output.
`prev_hash` is the hash of the previous json ipfs file.

1. `POST /`
*input:*
```json
{
  "content": "string"
}
```
*response:*
`SavedDocument`

2. `GET /uuid`
*response:*
`SavedDocument`
